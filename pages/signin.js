import React, { useEffect, useState } from 'react';

import { Box, Button, Grid, TextField, Typography, Paper } from '@material-ui/core';
import { makeStyles } from '@material-ui/core/styles';
import isEmail from 'is-email';
import { Magic } from 'magic-sdk';
import { useRouter } from 'next/router';

import { post } from 'src/fetch';
import { Page } from 'src/Page';

const useStyles = makeStyles(theme => ({
	form: {
		'& > *': {
			marginBottom: theme.spacing(2),
		},
		'& > *:last-child': {
			marginBottom: 0,
		},
	},
	header: {
		alignSelf: 'center',
	},
}));

// TODO: use LoadingButton instead of Button
// TODO: remove all email validation and rely on type="email"?
export default function SignIn() {
	const styles = useStyles();
	const router = useRouter();
	const [email, setEmail] = useState('');
	const [error, setError] = useState();
	const [loading, setLoading] = useState(false);
	const [magic, setMagic] = useState();

	useEffect(() => {
		router.prefetch('/');
	}, [router]);

	useEffect(() => {
		const m = new Magic(process.env.NEXT_PUBLIC_MAGIC_PUBLIC_KEY);
		m.preload();
		setMagic(m);
	}, []);

	useEffect(() => {
		(async () => {
			const ok = await fetch('/api/session').then(response => response.ok);
			if (ok) {
				router.replace('/');
			}
		})();
	}, [router]);

	function handleEmailChange(event) {
		setEmail(event.target.value);
	}

	async function handleSignIn(event) {
		event.preventDefault();
		setLoading(true);

		const trimmedEmail = email.trim();
		const isValid = trimmedEmail !== '' && isEmail(trimmedEmail);

		if (isValid) {
			setError('');
			const DIDT = await magic.auth.loginWithMagicLink({ email });

			try {
				await post('/api/session', { DIDT });
				router.replace('/');
			} catch (error) {
				// TODO: pass up to user
				console.error(error);
			}
		} else {
			setError('Please enter a valid email address');
		}

		setLoading(false);
		return false;
	}

	return (
		<Page
			isVerticallyCentered
			hideBar
			width="xs"
		>
			<Grid item className={styles.header}>
				<Box mb={1}>
					<Typography variant="h1">Owl Do</Typography>
				</Box>
			</Grid>

			<Grid item>
				<Paper>
					<Box p={2}>
						<form noValidate className={styles.form}>
							<TextField
								required
								fullWidth
								disabled={loading}
								type="email"
								label="Email"
								error={Boolean(error)}
								helperText={error}
								variant="outlined"
								value={email}
								onChange={handleEmailChange}
							/>
							<Button
								fullWidth
								disabled={loading}
								color="primary"
								variant="contained"
								type="submit"
								onClick={handleSignIn}
							>
								{loading ? 'Loading...' : 'Sign in'}
							</Button>
						</form>
					</Box>
				</Paper>
			</Grid>
		</Page>
	);
}
