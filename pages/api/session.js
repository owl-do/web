const { Magic, SDKError: MagicSDKError } = require('@magic-sdk/admin');
const HTTPError = require('node-http-error');
const { object, string } = require('superstruct');

const { db } = require('pages/api/_db');
const { withMiddlewares } = require('pages/api/_withMiddlewares');

const magic = new Magic(process.env.MAGIC_SECRET_KEY);

async function getSession(request) {
	try {
		const { id } = request.auth.claims;
		const user = await db.get(`user ${id}`);
		return user;
	} catch (error) {
		if (error.status === 404) {
			throw new HTTPError(401);
		}
	}
}

async function postSession(request, response) {
	const { DIDT } = request.body;

	try {
		await magic.token.validate(DIDT);
	} catch (error) {
		if (error instanceof MagicSDKError && error.code === 'ERROR_MALFORMED_TOKEN') {
			throw new HTTPError(400, 'Malformed token');
		}

		throw error;
	}

	const [_, { iss: id }] = magic.token.decode(DIDT);
	response.auth.set({ id });

	try {
		return await db.get(`user ${id}`);
	} catch (error) {
		if (error.status !== 404) {
			throw error;
		}
	}

	const user = {
		_id: `user ${id}`,
		classes: [],
		completed: {},
		theme: 'system',
	};

	await db.put(user);
	return user;
}

const postSchema = object({
	DIDT: string(),
});

async function deleteSession(request, response) {
	response.auth.clear();

	if (request.auth.claims) {
		const { id } = request.auth.claims;
		const metadata = await magic.users.getMetadataByIssuer(id);
		await magic.users.logoutByToken(metadata.publicAddress);
	}
}

module.exports = withMiddlewares({
	DELETE: {
		handler: deleteSession,
	},
	GET: {
		auth: {
			protect: true,
		},
		handler: getSession,
	},
	POST: {
		handler: postSession,
		schema: postSchema,
	},
	auth: {
		protect: false,
	},
});
