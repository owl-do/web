const HTTPError = require('node-http-error');
const { StructError, assert } = require('superstruct');

const withSchema = (next, schema) => async (request, response) => {
	if (schema) {
		try {
			assert(request.body, schema);
		} catch (error) {
			if (error instanceof StructError) {
				const httpError = new HTTPError(400);
				httpError.extra = `Expected '${error.path.join('.')}' to be of type '${error.type}' but received '${error.value}'.`;
				throw httpError;
			} else {
				throw error;
			}
		}
	}

	return next(request, response);
};

module.exports = {
	withSchema,
};
