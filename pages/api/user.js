const { db } = require('pages/api/_db');
const { User } = require('pages/api/_types');
const { withMiddlewares } = require('pages/api/_withMiddlewares');

async function getUser(request) {
	const { id } = request.auth.claims;
	const user = await db.get(`user ${id}`);
	return user;
}

async function postUser(request) {
	const { id } = request.auth.claims;
	const doc = await db.get(`user ${id}`);

	const newDoc = { ...doc, ...request.body };

	await db.put(newDoc);
	return newDoc;
}

const postSchema = User;

module.exports = withMiddlewares({
	GET: {
		handler: getUser,
	},
	POST: {
		handler: postUser,
		schema: postSchema,
	},
	auth: {
		protect: true,
	},
});
