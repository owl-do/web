const { array, define, enums, min, number, object, string, union } = require('superstruct');

const { alphabet, length } = require('pages/api/_id');

const Id = define('Id', id => typeof id === 'string' && new RegExp(`^[${alphabet}]{${length}}$`).test(id));

const PositiveNumber = min(number(), 0);

const Due = object({
	date: PositiveNumber,
	time: union([
		PositiveNumber,
		enums(['startOfClass', 'endOfDay']),
	]),
});

const Theme = enums(['system', 'dark', 'light']);

const Assignment = object({
	due: Due,
	name: string(),
});

const Class = object({
	instructor: Id,
	name: string(),
});

const Instructor = object({
	name: string(),
});

const User = object({
	classes: array(Id),
	completed: array(Id),
	theme: Theme,
});

module.exports = {
	Assignment,
	Class,
	Due,
	Id,
	Instructor,
	PositiveNumber,
	Theme,
	User,
};
