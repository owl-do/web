const { withAuth } = require('pages/api/_withAuth');
const { withMethods } = require('pages/api/_withMethods');
const { withSchema } = require('pages/api/_withSchema');
const { withSimpleResponse } = require('pages/api/_withSimpleResponse');

const withMiddlewares = function (options) {
	const handlers = {};
	const generalOptions = {};

	for (const [key, value] of Object.entries(options)) {
		((key.toUpperCase() === key) ? handlers : generalOptions)[key] = value;
	}

	for (const [method, { handler, ...options }] of Object.entries(handlers)) {
		const auth = options.auth || generalOptions.auth;
		const schema = options.schema || generalOptions.schema;

		handlers[method] = withAuth(withSchema(handler, schema), auth);
	}

	return withSimpleResponse(withMethods(handlers));
};

module.exports = {
	withMiddlewares,
};
