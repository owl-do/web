const HTTPError = require('node-http-error');
const { partial } = require('superstruct');

const { Instructor } = require('pages/api/_types');

const { db } = require('../_db');
const { withMiddlewares } = require('../_withMiddlewares');

async function deleteInstructor(request) {
	const { instructorId } = request.query;

	const instructor = await db.get(`instructor ${instructorId}`);
	instructor._deleted = true;
	await db.put(instructor);
}

async function getInstructor(request) {
	const { instructorId } = request.query;

	const instructor = await db.get(`instructor ${instructorId}`);

	if (instructor) {
		return instructor;
	}

	throw new HTTPError(404);
}

async function patchInstructor(request) {
	const { instructorId } = request.query;

	const instructor = await db.get(`instructor ${instructorId}`);

	if (!instructor) {
		throw new HTTPError(404);
	}

	await db.put({
		...instructor,
		...request.body,
	});

	return instructor;
}

const patchSchema = partial(Instructor);

module.exports = withMiddlewares({
	DELETE: {
		handler: deleteInstructor,
	},
	GET: {
		auth: { admin: false },
		handler: getInstructor,
	},
	PATCH: {
		handler: patchInstructor,
		schema: patchSchema,
	},
	auth: {
		admin: true,
	},
});
