const { db } = require('pages/api/_db');
const { getId } = require('pages/api/_id');
const { Instructor } = require('pages/api/_types');
const { withMiddlewares } = require('pages/api/_withMiddlewares');

async function putInstructor(request) {
	const instructor = {
		_id: `instructor ${getId()}`,
		...request.body,
	};

	await db.put(instructor);

	return instructor;
}

const putSchema = Instructor;

module.exports = withMiddlewares({
	PUT: {
		handler: putInstructor,
		schema: putSchema,
	},
	auth: {
		admin: true,
	},
});
