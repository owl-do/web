const jwt = require('jsonwebtoken');
const HTTPError = require('node-http-error');

const EXP = 86400 * 14; // 14 days
const REFRESH = 60 * 15; // 15 minutes

function expire(expiresIn) {
	return Math.floor(Date.now() / 1000) + expiresIn;
}

function sendToken(response, { token, expiresIn = EXP }) {
	const cookie = [`token=${token}`, 'Path=/api', 'SameSite=Strict', `Max-Age=${expiresIn}`];

	if (process.env.NODE_ENV === 'production') {
		cookie.concat('HttpOnly', 'Secure');
	}

	response.setHeader('Set-Cookie', cookie.join(';'));
}

function set(response, claims) {
	const token = jwt.sign({
		...claims,
		exp: expire(EXP),
		refresh: expire(REFRESH),
	}, process.env.SECRET);
	sendToken(response, { token });
}

function clear(request, response) {
	if (request.cookies.token) {
		sendToken(response, { expiresIn: -1, token: '' });
	}
}

function handleAuth(request, response) {
	const { token } = request.cookies;

	if (!token) {
		throw new Error('no_token');
	}

	const claims = jwt.verify(token, process.env.SECRET);

	if (claims.refresh <= expire(0)) {
		response.auth.set(claims);
	}

	return claims;
}

const withAuth = (next, options = {}) => (request, response) => {
	const {
		protect = true,
	} = options;

	response.auth = {
		clear: () => clear(request, response),
		set: claims => set(response, claims),
	};

	if (protect) {
		const claims = do {
			try {
				handleAuth(request, response);
			} catch {
				// TODO: pass up error to user and ask to re-auth
				throw new HTTPError(401);
			}
		};

		request.auth = {
			claims,
		};
	} else {
		request.auth = {};
	}

	return next(request, response);
};

module.exports = {
	withAuth,
};
