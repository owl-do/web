const HTTPError = require('node-http-error');

const { sanitizeDoc } = require('pages/api/_db');

const withSimpleResponse = next => async (request, response) => {
	try {
		const raw = await next(request, response);

		// Already responded to by next()
		if (response.finished) {
			return;
		}

		const sanitized = do {
			/* eslint-disable no-unused-expressions -- does not recognize do expressions */
			if (Array.isArray(raw)) {
				if (raw[0] && raw[0]._id) {
					raw.map(doc => sanitizeDoc(doc));
				} else {
					raw;
				}
			} else {
				raw && raw._id ? sanitizeDoc(raw) : raw;
			}
			/* eslint-enable no-unused-expressions */
		};

		response.status(200).send(sanitized);
	} catch (error) {
		let finalError = error;

		if (!(error instanceof HTTPError)) {
			const { body, headers, method, url } = request;

			const report = {
				date: new Date().toString(),
				error,
				request: {
					body,
					headers,
					method,
					url,
				},
			};

			console.error(report);
			finalError = new HTTPError(500);
		}

		response.status(finalError.status).send({
			extra: finalError.extra,
			message: finalError.message,
		});
	}
};

module.exports = {
	withSimpleResponse,
};
