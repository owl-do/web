const { db } = require('pages/api/_db');
const { getId } = require('pages/api/_id');
const { Class } = require('pages/api/_types');
const { withMiddlewares } = require('pages/api/_withMiddlewares');

async function getClasses() {
	const { rows } = await db.allDocs({
		endkey: 'class \uFFFF',
		include_docs: true, // eslint-disable-line camelcase
		startkey: 'class ',
	});

	return rows.map(({ doc }) => doc);
}

async function putClass(request) {
	const class_ = {
		_id: `class ${getId()}`,
		...request.body,
	};

	await db.put(class_);

	return class_;
}

const putSchema = Class;

module.exports = withMiddlewares({
	GET: {
		auth: { protect: false },
		handler: getClasses,
	},
	PUT: {
		auth: { admin: true },
		handler: putClass,
		schema: putSchema,
	},
});
