const { db } = require('pages/api/_db');
const { withMiddlewares } = require('pages/api/_withMiddlewares');

// TODO: add query verification
async function getClasses(request) {
	const { classes } = request.query;

	const { rows } = await db.allDocs({
		include_docs: true, // eslint-disable-line camelcase
		keys: classes.split(',').map(classId => `class ${classId}`),
	});

	return rows.map(({ doc }) => doc);
}

module.exports = withMiddlewares({
	GET: {
		handler: getClasses,
	},
});
