const { db } = require('pages/api/_db');
const { withMiddlewares } = require('pages/api/_withMiddlewares');

async function getAssignments(request) {
	const { classes } = request.query;

	const results = do {
		const promises = [];

		for (const classId of classes.split(',')) {
			const baseKey = `assignment ${classId} `;
			promises.push(db.allDocs({
				endkey: baseKey + '\uFFFF',
				include_docs: true, // eslint-disable-line camelcase
				startkey: baseKey,
			}));
		}

		await Promise.all(promises);
	};

	return results.map(({ rows }) =>
		rows.map(({ doc }) => doc),
	).flat();
}

module.exports = withMiddlewares({
	GET: {
		handler: getAssignments,
	},
});
