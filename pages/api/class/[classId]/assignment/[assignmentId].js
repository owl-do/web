const HTTPError = require('node-http-error');
const { partial } = require('superstruct');

const { db } = require('pages/api/_db');
const { Assignment } = require('pages/api/_types');
const { withMiddlewares } = require('pages/api/_withMiddlewares');

async function deleteAssignment(request) {
	const { assignmentId, classId } = request.query;

	const assignment = await db.get(`assignment ${classId} ${assignmentId}`);
	assignment._deleted = true;
	await db.put(assignment);
}

async function getAssignment(request) {
	const { assignmentId, classId } = request.query;

	const assignment = await db.get(`assignment ${classId} ${assignmentId}`);

	if (assignment) {
		return assignment;
	}

	throw new HTTPError(404);
}

async function patchAssignment(request) {
	const { assignmentId, classId } = request.query;

	const assignment = await db.get(`assignment ${classId} ${assignmentId}`);

	if (!assignment) {
		throw new HTTPError(404);
	}

	await db.put({
		...assignment,
		...request.body,
	});

	return assignment;
}

const patchSchema = partial(Assignment);

module.exports = withMiddlewares({
	DELETE: {
		handler: deleteAssignment,
	},
	GET: {
		auth: {
			admin: false,
		},
		handler: getAssignment,
	},
	PATCH: {
		handler: patchAssignment,
		schema: patchSchema,
	},
	auth: {
		admin: true,
		classPerm: ['assignment'],
	},
});
