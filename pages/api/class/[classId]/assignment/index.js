const { db } = require('pages/api/_db');
const { getId } = require('pages/api/_id');
const { Assignment } = require('pages/api/_types');
const { withMiddlewares } = require('pages/api/_withMiddlewares');

async function getAssignments(request) {
	const { classId } = request.query;

	const baseKey = `assignment ${classId} `;
	const { rows } = await db.allDocs({
		endkey: baseKey + '\uFFFF',
		include_docs: true, // eslint-disable-line camelcase
		startkey: baseKey,
	});

	return rows.map(({ doc }) => doc);
}

async function putAssignment(request) {
	const { classId } = request.query;

	const assignment = {
		_id: `assignment ${classId} ${getId()}`,
		class: classId,
		...request.body,
	};

	await db.put(assignment);

	return assignment;
}

const putSchema = Assignment;

module.exports = withMiddlewares({
	GET: {
		auth: {
			protect: false,
		},
		handler: getAssignments,
	},
	PUT: {
		auth: {
			admin: true,
			classRoles: ['assignment'],
		},
		handler: putAssignment,
		schema: putSchema,
	},
});
