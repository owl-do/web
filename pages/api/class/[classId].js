const HTTPError = require('node-http-error');
const { partial } = require('superstruct');

const { db } = require('pages/api/_db');
const { Class } = require('pages/api/_types');
const { withMiddlewares } = require('pages/api/_withMiddlewares');

async function deleteClass(request) {
	const { classId } = request.query;

	const class_ = await db.get(`class ${classId}`);
	class_._deleted = true;
	await db.put(class_);
}

async function getClass(request) {
	const { classId } = request.query;

	const class_ = await db.get(`class ${classId}`);

	if (class_) {
		return class_;
	}

	throw new HTTPError(404);
}

async function patchClass(request) {
	const { classId } = request.query;

	const class_ = await db.get(`class ${classId}`);

	if (!class_) {
		throw new HTTPError(404);
	}

	await db.put({
		...class_,
		...request.body,
	});

	return class_;
}

const patchSchema = partial(Class);

module.exports = withMiddlewares({
	DELETE: {
		handler: deleteClass,
	},
	GET: {
		auth: { admin: false },
		handler: getClass,
	},
	PATCH: {
		handler: patchClass,
		schema: patchSchema,
	},
	auth: {
		admin: true,
	},
});
