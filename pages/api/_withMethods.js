const HTTPError = require('node-http-error');

const withMethods = handlers => (request, response) => {
	const allMethods = [];

	for (const [method, next] of Object.entries(handlers)) {
		if (method === request.method) {
			return next(request, response);
		}

		allMethods.push(method);
	}

	response.setHeader('Allow', allMethods.join(', '));
	throw new HTTPError(405);
};

module.exports = {
	withMethods,
};
