const { db, sanitizeDocToEntry } = require('pages/api/_db');
const { withMiddlewares } = require('pages/api/_withMiddlewares');

async function getClasses(request) {
	const classIds = (request.query.classes ?? '').split(',');

	const options = classIds.length === 1 && classIds[0] === ''
		? {
			endkey: 'class \uFFFF',
			startkey: 'class ',
		}
		: {
			keys: classIds.map(id => `class ${id}`),
		};

	const entries = (await db.allDocs({
		include_docs: true, // eslint-disable-line camelcase
		...options,
	})).rows.map(doc => sanitizeDocToEntry(doc));

	return Object.fromEntries(entries);
}

module.exports = withMiddlewares({
	GET: {
		auth: { protect: false },
		handler: getClasses,
	},
});
