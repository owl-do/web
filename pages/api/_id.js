const { customAlphabet } = require('nanoid');

const alphabet = '0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz';
const length = 5;

const getId = customAlphabet(alphabet, length);

module.exports = {
	alphabet,
	getId,
	length,
};
