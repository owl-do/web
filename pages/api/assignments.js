const HTTPError = require('node-http-error');

const { db, sanitizeDocToEntry } = require('pages/api/_db');
const { withMiddlewares } = require('pages/api/_withMiddlewares');

async function getAssignments(request) {
	const classIds = (request.query.classes ?? '').split(',');

	if (classIds.length === 1 && classIds[0] === '') {
		throw new HTTPError(400);
	}

	const entries = (await Promise.all(
		classIds.map(classId => db.allDocs({
			endkey: `assignment ${classId} \uFFFF`,
			include_docs: true, // eslint-disable-line camelcase
			startkey: `assignment ${classId} `,
		})),
	)).map(({ rows }) => rows
		.map(doc => sanitizeDocToEntry(doc)),
	).flat();

	return Object.fromEntries(entries);
}

module.exports = withMiddlewares({
	GET: {
		auth: { protect: false },
		handler: getAssignments,
	},
});
