const PouchDB = require('pouchdb');
const find = require('pouchdb-find');

PouchDB.plugin(find);

const host = process.env.DB_HOST;
const auth = {
	password: process.env.DB_PASSWORD,
	username: process.env.DB_USERNAME,
};

const db = new PouchDB(`${host}/data`, { auth });

function sanitizeDoc({ _id: id, _rev, ...doc }) {
	return {
		...doc,
		id: id.split(' ').pop(),
	};
}

function sanitizeDocToEntry({ doc: rawDoc }) {
	const { id, ...doc } = sanitizeDoc(rawDoc);
	return [id, doc];
}

module.exports = {
	db,
	sanitizeDoc,
	sanitizeDocToEntry,
};
