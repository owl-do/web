import crypto from 'crypto';
import React from 'react';

import { ServerStyleSheets } from '@material-ui/core/styles';
import NextDocument, { Head, Html, Main, NextScript } from 'next/document';

function cspHash(text) {
	const hash = crypto.createHash('sha256');
	hash.update(text);
	return `'sha-${hash.digest('base64')}'`;
}

class Document extends NextDocument {
	render() {
		const csp = [
			"default-src 'none'",
			"base-uri 'none'",
			"connect-src 'self'",
			"font-src 'self'",
			"form-action 'self'",
			"img-src 'self'",
			"manifest-src 'self'",
			"prefetch-src 'self'",
			`script-src 'self' ${cspHash(NextScript.getInlineScriptSource(this.props))}`,
			"style-src 'self' 'unsafe-inline'",
			"worker-src 'self'",
			"frame-ancestors 'none'",
		].join(';');

		return (
			<Html lang="en">
				<Head>
					<meta httpEquiv="Content-Security-Policy" content={csp} />
				</Head>
				<body>
					<Main />
					<NextScript />
				</body>
			</Html>
		);
	}
}

// `getInitialProps` belongs to `_document` (instead of `_app`),
// it's compatible with server-side generation (SSG).
Document.getInitialProps = async ctx => {
	// Render app and page and get the context of the page with collected side effects.
	const sheets = new ServerStyleSheets();
	const originalRenderPage = ctx.renderPage;

	ctx.renderPage = () =>
		originalRenderPage({
			enhanceApp: App => props => sheets.collect(<App {...props} />),
		});

	const initialProps = await NextDocument.getInitialProps(ctx);

	return {
		...initialProps,
		// Styles fragment is rendered after the app and page rendering finish.
		styles: [...React.Children.toArray(initialProps.styles), sheets.getStyleElement()],
	};
};

export default Document;
