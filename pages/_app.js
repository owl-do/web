import PropTypes from 'prop-types';
import React, { useEffect } from 'react';

import '@fontsource/roboto';
import { CssBaseline } from '@material-ui/core';
import Head from 'next/head';

import { UserProvider } from 'src/api';
import { dark } from 'src/theme';
import { ThemeProvider } from 'src/ThemeProvider';

function App({ Component, pageProps }) {
	useEffect(() => {
		// Remove the server-side injected CSS.
		const jssStyles = document.querySelector('#jss-server-side');
		if (jssStyles) {
			jssStyles.remove();
		}
	}, []);

	return (
		<>
			<Head>
				<meta charSet="utf-8" />
				<meta name="viewport" content="width=device-width,initial-scale=1,viewport-fit=cover" />
				<meta name="description" content="Description" />
				<meta name="keywords" content="Keywords" />
				<title>Owl Do</title>

				<link rel="manifest" href="/manifest.json" />
				<link
					href="/favicon-16x16.png"
					rel="icon"
					type="image/png"
					sizes="16x16"
				/>
				<link
					href="/favicon-32x32.png"
					rel="icon"
					type="image/png"
					sizes="32x32"
				/>
				<link rel="apple-touch-icon" href="/apple-icon.png" />
				{/* PWA primary color */}
				<meta name="theme-color" content={dark.palette.primary.main} />

				<link rel="preload" href="/api/user" as="fetch" crossOrigin="use-credentials" />
			</Head>
			<UserProvider>
				<ThemeProvider>
					<CssBaseline />
					<Component {...pageProps} />
				</ThemeProvider>
			</UserProvider>
		</>
	);
}

App.propTypes = {
	Component: PropTypes.elementType.isRequired,
	pageProps: PropTypes.object.isRequired,
};

export default App;
