import React, { useState } from 'react';

import { Box, Grid, Paper, Slide } from '@material-ui/core';
import { Add, FilterList } from '@material-ui/icons';

import { useAssignments, useUser } from 'src/api';
import { AssignmentList } from 'src/AssignmentList';
import { Filters } from 'src/Filters';
import { Page } from 'src/Page';
import { withRequiredAuth } from 'src/withRequiredAuth';

function Dashboard() {
	const { user, userLoading } = useUser();
	const { assignments, assignmentsLoading } = useAssignments(user.classes);

	const [filterVisible, setFilterVisible] = useState(false);
	const toggleFilter = () => setFilterVisible(filterVisible => !filterVisible);

	const list = (user?.classes?.length ?? 1) === 0
		? <Box p={2}>It looks like you aren’t enrolled in any classes. Sign in or go to settings?</Box>
		: ((assignments?.length ?? 1) === 0
			? <Box p={2}>No assignments!</Box>
			: <AssignmentList isLoading={userLoading || assignmentsLoading} assignments={assignments ?? {}} />);

	return (
		<Page
			title="Due Dates"
			width="sm"
			actions={[
				{
					action: () => alert('Unimplemented'),
					icon: <Add />,
					label: 'add assignment',
					show: user.canAddAssignments,
				},
				{
					action: toggleFilter,
					icon: <FilterList />,
					label: 'filter assignment list',
				},
			]}
		>
			<Slide mountOnEnter unmountOnExit direction="down" in={filterVisible}>
				<Grid item>
					<Filters />
				</Grid>
			</Slide>

			<Grid item>
				<Paper>
					{ list }
				</Paper>
			</Grid>
		</Page>
	);
}

export default withRequiredAuth(Dashboard);
