import PropTypes from 'prop-types';
import React, { useState } from 'react';

import { Dialog, DialogActions, DialogContent, DialogContentText, DialogTitle, Button, Typography, Grid, Paper, TextField } from '@material-ui/core';
import { ToggleButton, ToggleButtonGroup, Autocomplete } from '@material-ui/lab';
import { makeStyles } from '@material-ui/styles';

import { useAllClasses, useUser } from 'src/api';
import { Page } from 'src/Page';
import { withRequiredAuth } from 'src/withRequiredAuth';

const useStyles = makeStyles(theme => ({
	buttonContainer: {
		'& > * + *': {
			marginLeft: theme.spacing(2),
		},
	},
	container: {
		'& > * + *': {
			marginTop: theme.spacing(2),
		},

		padding: theme.spacing(2),
	},
}));

function AutocompletePaper(props) {
	return <Paper {...props} elevation={3} />;
}

function LabelledSetting({ children, label }) {
	return (
		<Grid container justify="space-between" alignItems="baseline">
			<Grid item component={Typography} variant="body1">{label}</Grid>
			<Grid item >{children}</Grid>
		</Grid>
	);
}

LabelledSetting.propTypes = {
	children: PropTypes.node.isRequired,
	label: PropTypes.node.isRequired,
};

function Settings(props) {
	const styles = useStyles(props);

	const { allClasses = {}, allClassesLoading } = useAllClasses();
	const { resetUser, updateUser, user } = useUser();

	const handleClasses = (_event, classes) => updateUser({ classes });
	const handleTheme = (_event, theme) => updateUser({ theme });

	const [clearStatePromptOpen, setClearStatePromptOpen] = useState(false);
	const handleOpenClearStatePrompt = () => setClearStatePromptOpen(true);
	const handleCloseClearStatePrompt = () => setClearStatePromptOpen(false);
	const handleClearState = async () => {
		await Promise.all([
			resetUser(),
			localStorage.clear(),
		]);

		// TODO: loading button
		handleCloseClearStatePrompt();
	};

	return (
		<Page title="Settings" width="sm" backTo="/">
			<Grid item>
				<Paper className={styles.container}>
					<Autocomplete
						fullWidth
						multiple
						clearOnEscape
						includeInputInList
						filterSelectedOptions
						noOptionsText="No more classes"
						loading={allClassesLoading}
						options={Object.keys(allClasses)}
						getOptionLabel={id => allClasses[id]?.name ?? id}
						value={allClassesLoading ? [] : user.classes ?? []}
						renderInput={props => (
							<TextField
								{...props}
								label="Classes"
								placeholder="Filter by class names"
								variant="outlined"
							/>
						)}
						PaperComponent={AutocompletePaper}
						onChange={handleClasses}
					/>

					<LabelledSetting label="Theme">
						<ToggleButtonGroup
							exclusive
							value={user.theme}
							aria-label="theme"
							onChange={handleTheme}
						>
							<ToggleButton value="light">
								Light
							</ToggleButton>
							<ToggleButton value="system">
								System
							</ToggleButton>
							<ToggleButton value="dark">
								Dark
							</ToggleButton>
						</ToggleButtonGroup>
					</LabelledSetting>
				</Paper>
			</Grid>

			<Grid item>
				<Paper className={styles.container}>
					<LabelledSetting label="Clear app state">
						<Button onClick={handleOpenClearStatePrompt}>
							Clear
						</Button>
					</LabelledSetting>
				</Paper>
			</Grid>

			<Dialog
				open={clearStatePromptOpen}
				aria-labelledby="clear-state-prompt-title"
				aria-describedby="clear-state-prompt-description"
				onClose={handleCloseClearStatePrompt}
			>
				<DialogTitle id="clear-state-prompt-title">Clear all app state?</DialogTitle>
				<DialogContent>
					<DialogContentText id="clear-state-prompt-description">
						All settings, including enrolled classes will be deleted permanently.
						Data cached for offline operation will also be removed.
					</DialogContentText>
				</DialogContent>
				<DialogActions>
					<Button onClick={handleCloseClearStatePrompt}>
						Cancel
					</Button>
					<Button onClick={handleClearState}>
						Clear
					</Button>
				</DialogActions>
			</Dialog>
		</Page>
	);
}

export default withRequiredAuth(Settings);
