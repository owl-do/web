module.exports = {
	envs: [
		'browser',
		'commonjs',
	],
	extends: [
		'xo-react',
		'plugin:react-hooks/recommended',
		'plugin:jsx-a11y/strict',
	],
	ignores: 'public/sw.js',
	overrides: [
		{
			files: ['pages/api/**/*.js', '*.config.js'],
			rules: { 'import/no-commonjs': 'off' },
		},
		{
			files: [
				'pages/**/[^_]*.js',
				'pages/_app.js',
				'pages/_document.js',
			],
			rules: {
				'import/no-anonymous-default-export': [
					'error',
					{ allowArrowFunction: true },
				],
				'import/no-default-export': 'off',
				'import/prefer-default-export': 'error',
			},
		},
		{
			envs: ['browser', 'jest'],
			files: '*/*.test.js',
		},
		{
			envs: ['serviceworker', 'browser'],
			files: 'public/sw.js',
		},
	],
	parser: 'babel-eslint',
	plugins: [
		'sort-destructure-keys',
	],
	rules: {
		'capitalized-comments': [
			'error',
			'always',
			{ ignoreConsecutiveComments: true },
		],
		'comma-dangle': ['error', 'always-multiline'],
		eqeqeq: ['error', 'smart'],
		'import/exports-last': 'error',
		'import/extensions': ['error', 'never'],
		'import/group-exports': 'error',
		'import/no-commonjs': 'error',
		'import/no-default-export': 'error',
		'import/no-relative-parent-imports': 'error',
		'import/no-unassigned-import': [
			'error',
			{
				allow: ['@fontsource/*', 'preact/debug'],
			},
		],
		'import/order': [
			'error',
			{
				alphabetize: {
					caseInsensitive: true,
					order: 'asc',
				},
				groups: [
					'builtin',
					'external',
					'internal',
					'parent',
					'sibling',
					'index',
				],
				'newlines-between': 'always',
			},
		],
		'jsx-a11y/label-has-for': 'off',
		'no-eq-null': 'off',
		'no-multiple-empty-lines': [
			'error',
			{
				max: 1,
				maxBOF: 0,
				maxEOF: 0,
			},
		],
		'no-unused-vars': [
			'error',
			{
				args: 'all',
				argsIgnorePattern: '^_',
				caughtErrors: 'all',
				caughtErrorsIgnorePattern: '^_',
				ignoreRestSiblings: false,
				vars: 'all',
				varsIgnorePattern: '^_',
			},
		],
		'object-curly-spacing': ['error', 'always'],
		'operator-linebreak': ['error', 'before'],
		quotes: [
			'error',
			'single',
			{ avoidEscape: true },
		],
		'react/boolean-prop-naming': [
			'error',
			{ rule: '(^(is|has|hide|show)([A-Z][A-Za-z0-9])*)|^disabled$' },
		],
		'react/function-component-definition': [
			'error',
			{
				namedComponents: 'function-declaration',
				unnamedComponents: 'arrow-function',
			},
		],
		'react/jsx-tag-spacing': [
			'error',
			{ beforeSelfClosing: 'always' },
		],
		'sort-destructure-keys/sort-destructure-keys': [
			'error',
			{ caseSensitive: true },
		],
		'sort-keys': [
			'error',
			'asc',
			{
				caseSensitive: true,
				natural: true,
			},
		],
		'unicorn/filename-case': 'off',
		'unicorn/no-unsafe-regex': 'error',
		'unicorn/prefer-replace-all': 'error',
	},
	settings: {
		'import/core-modules': ['react', 'prop-types'],
		'import/internal-regex': '^(src|pages)/',
	},
};
