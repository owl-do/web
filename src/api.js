import PropTypes from 'prop-types';
import React, { createContext, useContext } from 'react';

import { get, post } from 'src/fetch';
import { useLocalSwr } from 'src/useLocalSwr';

const fetchWithClasses = (url, ...classes) => get(`${url}?classes=${classes.join(',')}`);

function useRename(newName, { data, isLoading, ...swr }) {
	return {
		[newName]: data,
		[`${newName}Loading`]: isLoading,
		...swr,
	};
}

function useAssignments(classes = []) {
	return useRename('assignments', useLocalSwr(
		classes.length === 0 ? null : ['/api/assignments', ...classes.sort()],
		{ fetcher: fetchWithClasses },
	));
}

function useAllClasses() {
	return useRename('allClasses', useLocalSwr('/api/classes'));
}

function useClasses(classes = []) {
	return useRename('classes', useLocalSwr(
		classes.length === 0 ? null : ['/api/classes', ...classes.sort()],
		{ fetcher: fetchWithClasses },
	));
}

const User = createContext();
const useUser = () => useContext(User);

function UserProvider({ children }) {
	const { error, mutate, user: swrUser, userLoading } = useRename('user', useLocalSwr('/api/user'));

	const user = {
		error,
		updateUser: async changes => {
			mutate({ ...user, ...changes }, false);
			const serverUser = await post('/api/user', changes);
			mutate(serverUser, false);
		},
		user: swrUser ? {
			...swrUser,
			canAddAssignments: do {
				const { admin, assignment = [] } = swrUser?.roles ?? {};
				admin || assignment.length > 0; // eslint-disable-line no-unused-expressions -- does not recognize do expressions
			},
		} : undefined,
		userLoading,
	};

	return (
		<User.Provider value={user}>
			{children}
		</User.Provider>
	);
}

UserProvider.propTypes = {
	children: PropTypes.node.isRequired,
};

export {
	UserProvider,
	useAllClasses,
	useAssignments,
	useClasses,
	useUser,
};
