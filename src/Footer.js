import React from 'react';

import { Grid, Link as MuiLink } from '@material-ui/core';
import { makeStyles } from '@material-ui/core/styles';
import { repository, bugs, version } from 'package.json';

const useStyles = makeStyles(theme => ({
	root: {
		color: theme.palette.text.disabled,
		fontSize: '0.75rem',
		margin: 0,
		width: '100%',
	},
}));

function Link(props) {
	return (
		<MuiLink color="inherit" target="_blank" rel="noreferrer" {...props} />
	);
}

function Footer(props) {
	const styles = useStyles(props);

	const [repoName, repoType = 'github'] = repository.split(':').reverse();
	const repoUrl = `https://${repoType}.com/${repoName}`;
	const versionUrl = `${repoUrl}/${repoType === 'gitlab' ? '-' : 'releases'}/tag/v${version}`;

	const versionLink = <Link href={versionUrl}>v{version}</Link>;
	const repoLink = <Link href={repoUrl}>{repoName}</Link>;
	const issuesLink = <Link href={bugs}>issues</Link>;
	const bugEmailLink = <Link href={`mailto:${process.env.NEXT_STATIC_SUPPORT_EMAIL}`}>email</Link>;

	return (
		<Grid
			{...props}
			container
			className={styles.root}
			component="footer"
			direction="row"
			spacing={1}
			justify="center"
			alignItems="flex-end"
		>
			<Grid item>{versionLink}</Grid>
			<Grid item>|</Grid>
			<Grid item>{repoLink}</Grid>
			<Grid item>|</Grid>
			<Grid item>{issuesLink} ({bugEmailLink})</Grid>
		</Grid>
	);
}

export {
	Footer,
};
