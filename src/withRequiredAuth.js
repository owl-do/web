import React from 'react';

import { CircularProgress } from '@material-ui/core';
import { makeStyles } from '@material-ui/core/styles';
import { useRouter } from 'next/router';

import { useUser } from 'src/api';
import { Page } from 'src/Page';

const useStyles = makeStyles({
	center: {
		alignItems: 'center',
		display: 'flex',
		height: '100vh',
		justifyContent: 'center',
		width: '100vw',
	},
});

function withRequiredAuth(WrappedComponent) {
	function WithRequiredAuth(props) {
		const styles = useStyles();
		const router = useRouter();
		const { error, user, userLoading } = useUser();

		if (userLoading) {
			return (
				<div className={styles.center}>
					<CircularProgress />
				</div>
			);
		}

		if (error && error?.status !== 401) {
			return (
				<Page isVerticallyCentered hasBar={false} width="sm">
					error: <pre><code>{JSON.stringify(error)}</code></pre>
				</Page>
			);
		}

		if (!user) {
			router.replace('/signin');
			return;
		}

		return <WrappedComponent {...props} />;
	}

	WithRequiredAuth.displayName = `WithRequiredAuth(${WrappedComponent.displayName ?? WrappedComponent.name ?? 'Component'})`;

	return WithRequiredAuth;
}

export {
	withRequiredAuth,
};
