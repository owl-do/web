import useSWR from 'swr';

import { get } from 'src/fetch';

function sanitizeKey(key) {
	return Array.isArray(key) ? key[0] : key;
}

function useLocalSwr(key, { onSuccess = () => {}, ...swrOptions } = {}) {
	const safeKey = sanitizeKey(key);
	const localKey = `swr ${safeKey}`;

	const initialData = (typeof window === 'undefined' || !key)
		? undefined
		: JSON.parse(localStorage.getItem(localKey)) ?? undefined;

	const { data, error, mutate } = useSWR(key, {
		fetcher: get,
		initialData,
		onSuccess(data, swrKey, config) {
			if (typeof window !== 'undefined') {
				// eslint-disable-next-line no-unused-expressions
				JSON.stringify(data) |> localStorage.setItem(localKey, ?);
			}

			onSuccess(data, swrKey, config);
		},
		shouldRetryOnError: false,
		...swrOptions,
	});

	return {
		data,
		error,
		isLoading: !data && !error,
		mutate,
	};
}

export {
	useLocalSwr,
};
