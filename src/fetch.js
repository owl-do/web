class APIError extends Error {
	constructor({ extra, message, status }, ...rest) {
		super(message, ...rest);

		this.name = this.constructor.name;
		this.status = status;
		this.extra = extra;
	}
}

async function simpleFetch(url, options) {
	const response = await fetch(url, options);
	const body = await response.json();

	if (response.ok) {
		return body;
	}

	throw new APIError({
		...body,
		status: response.status,
	});
}

const get = simpleFetch;

async function post(url, data, options) {
	const jsonMime = 'application/json';

	return simpleFetch(url, {
		...options,
		body: JSON.stringify(data),
		headers: {
			Accept: jsonMime,
			'Content-Type': jsonMime,
		},
		method: 'POST',
	});
}

export {
	APIError,
	get,
	post,
};
