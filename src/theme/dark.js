import { createMuiTheme } from '@material-ui/core/styles';
import { merge } from 'lodash';

import { baseTheme } from './common';

const theme = merge({}, baseTheme, {
	overrides: {
		MuiAppBar: {
			colorDefault: {
				backgroundColor: do {
					/* eslint-disable no-unused-expressions -- does not recognize do expressions */
					const x = 18 / baseTheme.overlayOpacity;
					`rgb(${x} ${x} ${x} / ${baseTheme.overlayOpacity})`;
					/* eslint-enable no-unused-expressions */
				},
			},
			root: {
				borderBottomStyle: 'solid',
				borderStyle: 'none',
			},
		},
	},
	palette: {
		background: {
			default: '#010101',
			paper: do {
				/* eslint-disable no-unused-expressions -- does not recognize do expressions */
				const x = 16 / baseTheme.overlayOpacity;
				`rgb(${x} ${x} ${x} / ${baseTheme.overlayOpacity})`;
				/* eslint-enable no-unused-expressions */
			},
		},
		error: {
			main: '#cf6679',
		},
		primary: {
			main: '#bb86fc',
		},
		primaryVariant: {
			main: '#3700b3',
		},
		secondary: {
			main: '#03dac6',
		},
		secondaryVariant: {
			main: '#03dac6',
		},
		type: 'dark',
	},
});

const dark = createMuiTheme(theme);

dark.overrides.MuiPaper = {
	root: {
		border: `1px solid ${dark.palette.divider}`,
	},
};

export {
	dark,
};
