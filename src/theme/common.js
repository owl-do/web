const hexToRgb = hex => (hex?.main ?? hex)
	.match(/[\da-f]{2}/g)
	.map(x => Number.parseInt(x, 16));

const rgbToHex = rgb => '#' + rgb.map(x => x.toString(16)).join('');

const safeAreas = Object.fromEntries(
	['top', 'right', 'bottom', 'left'].map(side => [
		side,
		fallback => `env(safe-area-inset-${side}${fallback ? `, ${fallback}` : ''}, 0px)`,
	]),
);

const blur = 'blur(10px)';

const baseTheme = {
	blur,
	overlayOpacity: 0.75,
	overrides: {
		MuiAppBar: {
			root: {
				backdropFilter: blur,
			},
		},
		MuiAutocomplete: {
			popper: {
				backdropFilter: blur,
			},
		},
	},
	safeAreas,
	shadows: new Array(25).fill(''),
};

export {
	hexToRgb,
	rgbToHex,
	baseTheme,
};
