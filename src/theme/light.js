import { createMuiTheme } from '@material-ui/core/styles';
import { merge } from 'lodash';

import { baseTheme } from './common';

const theme = merge({}, baseTheme, {
	palette: {
		type: 'light',
	},
});

const light = createMuiTheme(theme);
export {
	light,
};
