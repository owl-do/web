// TODO: handle timezones

const MS_PER_DAY = 8.64e7;

function formatDate(date) {
	const now = Math.floor(new Date() / MS_PER_DAY);

	switch (date - now) {
		case 0:
			return 'Today';
		case -1:
			return 'Yesterday';
		case 1:
			return 'Tomorrow';
		default:
	}

	// TODO: alternative formats for closer/further dates?
	const format = {
		day: '2-digit',
		month: 'short',
	};

	return new Intl.DateTimeFormat(undefined, {
		timeZone: 'UTC',
		...format,
	}).format(new Date(date * MS_PER_DAY));
}

function formatTime(time) {
	switch (time) {
		case 'endOfDay':
			return 'midnight';
		case 'startOfClass':
			return 'the beginning of class';
		default:
	}

	const format = x => x.toString().padStart(2, '0');
	const hours = Math.floor(time / 60);
	const minutes = time % 60;
	return `${format(hours)}:${format(minutes)}`;
}

function useDue(due) {
	if (!due) {
		return null;
	}

	return `${formatDate(due.date)} at ${formatTime(due.time)}`;
}

export {
	useDue,
};
