import PropTypes from 'prop-types';
import React from 'react';

import { Checkbox, FormControlLabel, Grid, Paper, Radio, RadioGroup, Typography } from '@material-ui/core';
import { makeStyles } from '@material-ui/core/styles';

import { useClasses, useUser } from 'src/api';

const useStyles = makeStyles(theme => ({
	filterBlock: {
		padding: theme.spacing(1),
		paddingLeft: theme.spacing(2),
		paddingRight: theme.spacing(2),
	},
	groupContent: {
		width: '100%',
	},
}));

function FilterBlock(props) {
	const { children, title, width } = props;
	const styles = useStyles(props);

	return (
		<Grid item xs={width}>
			<Paper className={styles.filterBlock}>
				<Grid container direction="column" alignItems="center" spacing={1}>
					<Grid item>
						<Typography variant="subtitle1">{title}</Typography>
					</Grid>
					<Grid item className={styles.groupContent}>
						{children}
					</Grid>
				</Grid>
			</Paper>
		</Grid>
	);
}

FilterBlock.propTypes = {
	children: PropTypes.node.isRequired,
	title: PropTypes.string.isRequired,
	width: PropTypes.number.isRequired,
};

function RadioFilterBlock({ filters, title, value, width, ...rest }) {
	return (
		<FilterBlock title={title} width={width}>
			<RadioGroup aria-label={title.toLowerCase()} value={value} {...rest}>
				{filters.map(([key, label]) => (
					<FormControlLabel
						key={key}
						control={<Radio />}
						value={key}
						label={label}
						color="secondary"
					/>
				))}
			</RadioGroup>
		</FilterBlock>
	);
}

RadioFilterBlock.propTypes = {
	filters: PropTypes.array.isRequired,
	title: PropTypes.string.isRequired,
	value: PropTypes.string.isRequired,
	width: PropTypes.number,
};

RadioFilterBlock.defaultProps = {
	width: 6,
};

// TODO: loading state for classes
function Filters() {
	const { user } = useUser();
	const { classes = {} } = useClasses(user.classes);

	const timeFilters = [
		['today', 'Today'],
		['week', 'This week'],
		['all', 'All time'],
	];

	const statusFilters = [
		['done', 'Done'],
		['todo', 'To-do'],
		['all', 'All'],
	];

	return (
		<Grid container spacing={2}>
			<RadioFilterBlock title="Due" value="all" filters={timeFilters} onChange={console.error} />
			<RadioFilterBlock title="Status" value="all" filters={statusFilters} onChange={console.error} />
			<FilterBlock title="Classes" width={12}>
				<Grid container justify="center" spacing={1}>
					{Object.entries(classes).map(([id, { name }]) => (
						<Grid key={id} item>
							<FormControlLabel
								checked
								control={<Checkbox color="secondary" />}
								label={name}
								onChange={console.error}
							/>
						</Grid>
					))}
				</Grid>
			</FilterBlock>
		</Grid>
	);
}

export {
	Filters,
};
