import PropTypes from 'prop-types';
import React from 'react';

import { List } from '@material-ui/core';

import { useUser, useClasses } from 'src/api';
import { AssignmentListItem } from 'src/AssignmentListItem';

function AssignmentList({ assignments, isLoading }) {
	const { updateUser, user, userLoading } = useUser();
	const { classes, classesLoading } = useClasses(user.classes);

	if (isLoading || userLoading || classesLoading) {
		return Array.from(
			{
				length: 3,
			},
			(_, x) => <AssignmentListItem key={x} isLoading />,
		);
	}

	const toggleCompleted = ({ target: { id } }) => {
		const { completed } = user;
		updateUser({
			completed: completed.includes(id)
				? completed.filter(x => x !== id)
				: [...completed, id],
		});
	};

	// Memoize?
	const assignmentSort = ([_idA, a], [_idB, b]) => {
		const compare = (a, b) => {
			if ((typeof a === 'string') && (typeof b === 'string')) {
				([a, b] = [a.toLowerCase(), b.toLowerCase()]);
			}

			if (a < b) {
				return -1;
			}

			if (a > b) {
				return 1;
			}

			return 0;
		};

		const dateComparison = compare(a.due.date, b.due.date);
		if (dateComparison !== 0) {
			return dateComparison;
		}

		const compareTimes = (a, b) => {
			if (a === b) {
				return 0;
			}

			if ((typeof a === 'number') && (typeof b === 'number')) {
				return compare(a, b);
			}

			if (a === 'startOfClass' || b === 'endOfDay') {
				return -1;
			}

			return 1;
		};

		const timeComparison = compareTimes(a.due.time, b.due.time);
		if (timeComparison === 0) {
			const classComparison = compare(classes[a.class].name, classes[b.class].name);
			if (classComparison === 0) {
				return compare(a.name, b.name);
			}

			return classComparison;
		}

		return timeComparison;
	};

	return (
		<List>
			{Object.entries(assignments).sort(assignmentSort).map(([id, { class: classId, due, name }]) => (
				<AssignmentListItem
					key={id}
					id={id}
					name={name}
					due={due}
					nameOfClass={classes[classId].name}
					isCompleted={Boolean(user.completed.includes(id))}
					toggleCompleted={toggleCompleted}
				/>
			))}
		</List>
	);
}

AssignmentList.propTypes = {
	assignments: PropTypes.object.isRequired,
	isLoading: PropTypes.bool.isRequired,
};

export {
	AssignmentList,
};
