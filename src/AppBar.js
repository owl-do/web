import PropTypes from 'prop-types';
import React from 'react';

import { IconButton, AppBar as MuiAppBar, Toolbar, Typography } from '@material-ui/core';
import { makeStyles } from '@material-ui/core/styles';
import { ArrowBack, Settings as SettingsIcon } from '@material-ui/icons';
import Link from 'next/link';

const useStyles = makeStyles(theme => ({
	backArrow: {
		marginRight: theme.spacing(2),
	},
	title: {
		flexGrow: 1,
	},
}));

function AppBar(props) {
	const { actions, backTo, title, ...otherProps } = props;
	const styles = useStyles(props);

	return (
		<MuiAppBar color="default" position="sticky" {...otherProps}>
			<Toolbar>
				{backTo && (
					<Link passHref href={backTo}>
						<IconButton
							edge="start"
							className={styles.backArrow}
							color="inherit"
						>
							<ArrowBack />
						</IconButton>
					</Link>
				)}

				<Typography variant="h6" className={styles.title}>
					{title}
				</Typography>

				{actions.filter(({ show = true }) => show).map(({ action, icon, label }) => (
					<IconButton
						key={label}
						aria-label={label}
						color="inherit"
						onClick={action}
					>
						{icon}
					</IconButton>
				))}

				<Link passHref href="/settings">
					<IconButton color="inherit" edge="end">
						<SettingsIcon />
					</IconButton>
				</Link>
			</Toolbar>
		</MuiAppBar>
	);
}

AppBar.defaultProps = {
	actions: [],
};

AppBar.propTypes = {
	actions: PropTypes.arrayOf(PropTypes.shape({
		action: PropTypes.func.isRequired,
		icon: PropTypes.node.isRequired,
		label: PropTypes.string.isRequired,
		show: PropTypes.bool,
	})),
	backTo: PropTypes.string,
	title: PropTypes.string.isRequired,
};

export {
	AppBar,
};
