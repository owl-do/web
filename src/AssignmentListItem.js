import PropTypes from 'prop-types';
import React, { memo } from 'react';

import { Box, Checkbox, Chip, ListItem, ListItemIcon, ListItemText } from '@material-ui/core';
import { makeStyles } from '@material-ui/core/styles';
import { Skeleton } from '@material-ui/lab';
import isRequiredIf from 'react-proptype-conditional-require';

import { useDue } from 'src/useDue';

const useStyles = makeStyles(theme => ({
	chip: {
		marginRight: theme.spacing(1),
	},
}));

const AssignmentListItem = memo(function AssignmentListItem(props) {
	const { due, id, isCompleted, isLoading, name, nameOfClass, toggleCompleted } = props;
	const styles = useStyles(props);

	const dueString = useDue(due);

	return (
		<ListItem>
			<ListItemIcon>
				{isLoading ? null : (
					<Checkbox
						disableRipple
						id={id}
						checked={isCompleted}
						color="primary"
						tabIndex={-1}
						onChange={toggleCompleted}
					/>
				)}
			</ListItemIcon>

			<ListItemText
				primary={isLoading ? <Skeleton /> : (
					<label htmlFor={id}>
						<Box display="inline">
							<Chip
								className={styles.chip}
								size="small"
								label={nameOfClass}
							/>
						</Box>
						{name}
					</label>
				)}
				secondary={isLoading ? <Skeleton width="25%" /> : dueString}
			/>
		</ListItem>
	);
});

const isRequiredIfLoaded = validator => isRequiredIf(validator, props => !props.isLoading);

AssignmentListItem.propTypes = {
	nameOfClass: isRequiredIfLoaded(PropTypes.string),
	due: isRequiredIfLoaded(PropTypes.shape({
		date: PropTypes.number.isRequired,
		time: PropTypes.oneOf([
			PropTypes.number,
			'startOfClass',
			'endOfDay',
		]).isRequired,
	})),
	id: isRequiredIfLoaded(PropTypes.string),
	isCompleted: isRequiredIfLoaded(PropTypes.bool),
	isLoading: PropTypes.bool.isRequired,
	name: isRequiredIfLoaded(PropTypes.string),
	toggleCompleted: isRequiredIfLoaded(PropTypes.func),
};

export {
	AssignmentListItem,
};
