import PropTypes from 'prop-types';
import React from 'react';

import { useMediaQuery } from '@material-ui/core';
import { ThemeProvider as MuiThemeProvider } from '@material-ui/core/styles';

import { useUser } from 'src/api';
import { dark, light } from 'src/theme';

function ThemeProvider({ children }) {
	const systemDark = useMediaQuery('(prefers-color-scheme: dark)');
	const userSetting = useUser().user?.theme ?? 'system';

	const theme = (
		userSetting === 'system'
			? systemDark
			: userSetting === 'dark'
	) ? dark : light;

	return <MuiThemeProvider theme={theme}>{children}</MuiThemeProvider>;
}

ThemeProvider.propTypes = {
	children: PropTypes.node.isRequired,
};

export {
	ThemeProvider,
};
