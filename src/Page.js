import PropTypes from 'prop-types';
import React from 'react';

import { Container } from '@material-ui/core';
import { makeStyles } from '@material-ui/core/styles';

import { AppBar } from 'src/AppBar';
import { Footer } from 'src/Footer';

// TODO: pad AppBar with safe areas

const useStyles = makeStyles(theme => ({
	'@global': {
		'#__next': {
			display: 'flex',
			flexDirection: 'column',
			height: '100%',
		},
		'html, body': {
			height: '100%',
		},
	},
	bar: {
		marginBottom: theme.spacing(2),
	},
	footer: {
		marginTop: theme.spacing(2),
	},
	main: {
		'& > * + *': {
			marginTop: theme.spacing(2),
		},

		display: 'flex',
		flex: '1 0 auto',
		flexDirection: 'column',
		justifyContent: ({ isVerticallyCentered }) => isVerticallyCentered ? 'center' : 'normal',
	},
}));

function Page(props) {
	const { actions, backTo, children, hideBar, hideFooter, title, width } = props;
	const styles = useStyles(props);

	return (
		<>
			{ !hideBar && <AppBar className={styles.bar} title={title} actions={actions} backTo={backTo} /> }
			<Container
				maxWidth={width}
				component="main"
				className={styles.main}
			>
				{children}
			</Container>
			{ !hideFooter && <Footer className={styles.footer} /> }
		</>
	);
}

Page.propTypes = {
	actions: PropTypes.array,
	backTo: PropTypes.string,
	children: PropTypes.node.isRequired,
	hideBar: PropTypes.bool,
	hideFooter: PropTypes.bool,
	isVerticallyCentered: PropTypes.bool, // eslint-disable-line react/no-unused-prop-types -- used in makeStyles
	title: PropTypes.string,
	width: PropTypes.oneOf(['xs', 'sm', 'md', 'lg', 'xl', false]).isRequired,
};

Page.defaultProps = {
	hideBar: false,
	hideFooter: false,
	isVerticallyCentered: false,
	title: '',
};

export {
	Page,
};
