const bundleAnalyzer = require('@next/bundle-analyzer');
const prefresh = require('@prefresh/next');
const withPlugins = require('next-compose-plugins');
const env = require('next-env');
const pwa = require('next-pwa');

module.exports = withPlugins([
	bundleAnalyzer({
		enabled: process.env.ANALYZE === 'true',
	}),
	prefresh,
	[pwa, {
		pwa: {
			dest: 'public',
		},
	}],
	env,
], {
	async headers() {
		const format = headers => Object.entries(headers).map(([key, value]) => ({
			key,
			value: Array.isArray(value)
				? value.join('; ')
				: value,
		}));

		return [
			{
				headers: format({
					'Referrer-Policy': 'no-referrer',
					'X-Content-Type-Options': 'nosniff',
					'X-Frame-Options': 'deny',
					'X-UA-Compatible': 'IE=edge',
					'X-XSS-Protection': [
						'1',
						'mode=block',
					],
				}),
				source: '/(.*)',
			},
			{
				headers: format({
					'Content-Security-Policy': [
						"default-src 'none'",
						"frame-ancestors 'none'",
					],
				}),
				source: '/api/(.*)',
			},
		];
	},
	poweredByHeader: false,
	reactStrictMode: true,
	webpack(config, { dev, isServer }) {
		// Move Preact into the framework chunk instead of duplicating in routes:
		const splitChunks = config.optimization && config.optimization.splitChunks;
		if (splitChunks) {
			const cacheGroups = splitChunks.cacheGroups;
			const test = /[\\/]node_modules[\\/](preact|preact-render-to-string|preact-context-provider)[\\/]/;
			if (cacheGroups.framework) {
				cacheGroups.preact = Object.assign({}, cacheGroups.framework, { test });
				// If you want to merge the 2 small commons+framework chunks:
				cacheGroups.commons.name = 'framework';
			}
		}

		if (isServer) {
			// Mark `preact` stuffs as external for server bundle to prevent duplicate copies of preact
			config.externals.push(/^(preact|preact-render-to-string|preact-context-provider)([\\/]|$)/);
		}

		// Install webpack aliases:
		const aliases = config.resolve.alias || (config.resolve.alias = {});
		aliases.react = 'preact/compat';
		aliases['react-dom'] = 'preact/compat';

		// Automatically inject Preact DevTools:
		if (dev && !isServer) {
			const entry = config.entry;
			config.entry = () =>
				entry().then(entries => {
					entries['main.js'] = ['preact/debug'].concat(entries['main.js'] || []);
					return entries;
				});
		}

		return config;
	},
});
